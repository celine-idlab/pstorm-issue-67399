FROM php:7.4-cli AS php_cli

RUN apt-get update && apt-get install -y libssl-dev zlib1g-dev libzip-dev git subversion mercurial && rm -rf /var/lib/apt/lists/*
RUN docker-php-ext-install zip && docker-php-ext-enable zip
RUN apt-get install pkg-config

RUN apt-get update \
  && apt-get install -y libmcrypt-dev \
  && apt-get install -y libicu-dev \
  && apt-get install -y libcurl3-dev \
  && apt-get install -y g++ \
  && apt-get install -y locales \
  && apt-get install -y tzdata \
  && apt-get install -y libpng-dev \
  && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install \
    pdo \
    pdo_mysql \
    intl \
    gd
RUN docker-php-ext-enable \
    pdo \
    pdo_mysql \
    intl \
    gd
RUN docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd

RUN apt-get update && apt-get install -y libpcre3-dev && rm -rf /var/lib/apt/lists/*
RUN pecl install oauth
RUN docker-php-ext-enable oauth

RUN apt-get update && apt-get install -y libpq-dev && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql

COPY memory.ini /usr/local/etc/php/conf.d/memory.ini

WORKDIR /app


FROM php_cli AS php_cli_xdebug

RUN pecl install xdebug-3.0.2 && docker-php-ext-enable xdebug

COPY xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini

WORKDIR /app


FROM php_cli AS php_cli_composer

RUN apt-get update && apt-get install -y zip unzip && rm -rf /var/lib/apt/lists/*

USER root

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /tmp

RUN curl -o composer-setup.php https://getcomposer.org/installer \
 && php composer-setup.php --no-ansi --install-dir=/usr/bin --filename=composer --quiet \
 && composer --ansi --version --no-interaction \
 && rm composer-setup.php

WORKDIR /app


FROM php_cli AS php_cli_pcov

RUN pecl install pcov && docker-php-ext-enable pcov

WORKDIR /app