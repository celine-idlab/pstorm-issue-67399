## Build PHP docker images with appropriate extensions

```
docker build --target php_cli -t local/wi-67399/php_cli:latest .
docker build --target php_cli_xdebug -t local/wi-67399/php_cli_xdebug:latest .
docker build --target php_cli_composer -t  local/wi-67399/php_cli_composer:latest .
```

## Install dependencies (phpunit)

```
docker compose -f docker-compose.override.dist.yml run composer composer install
```

## Verify it works

```
docker compose -f docker-compose.override.dist.yml run automated-tests-xdebug vendor/bin/phpunit tests/IntUtilsTest.php
```

Result must be in success :

```
PHPUnit 9.5.21 #StandWithUkraine

.                                                                   1 / 1 (100%)

Time: 00:00.003, Memory: 4.00 MB

OK (1 test, 1 assertion)
```


## Configure PHPStorm

### PHP Interpreter

File -> Settings -> PHP -> click the 3 dots near CLI Interpreter -> click the 'plus' button -> From Docker, Vagrant etc. -> choose Docker Compose, select the docker-compose.override.dist.yml file and select automated-tests-xdebug service :

![config](./php-cli-configuration-with-alternative-docker-compose-file.png)

Check PHP Version and XDebug version are

![success](./php-cli-configuration-result.png)

### Test runner

File -> Settings -> PHP -> Test Frameworks -> click the 'plus' button -> PHPUnit by remote interpreter -> select automated-tests-xdebug interpreter -> select the option 'path to PHPUnit.phar' and set it to '/app/vendor/bin/phpunit'

Check PHPUnit Version is successfully identified by PSTorm

![success](./phpunit-configuration.png)

Can't reproduce, I click on the 'run' button, PStorm runs the expected command :

![can reproduce](path-in-configuration-becomes-incorrect.png)
![cannot reproduce](still-cannot-reproduce.png)

Please note that path mapping is correct in CLI Interpreter and Test Frameworks sections :
![still correct here](path-mapping-is-still-correct.png)
