<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;

class IntUtilsTest extends TestCase
{

    public function testSum()
    {
        $expected = 5;
        $actual = (new \App\IntUtils())->sum(2,3);
        $this->assertEquals($expected, $actual);
    }
}